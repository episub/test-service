FROM golang:1.8-alpine

ADD . /go/src/bitbucket.org/episub/test-service
RUN go install bitbucket.org/episub/test-service

ENTRYPOINT test-service

EXPOSE 8080
