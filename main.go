package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

const version = "1.3"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		host, err := os.Hostname()

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "Version: %s\nHost: %s", version, host)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
